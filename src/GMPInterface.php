<?php

namespace App;


interface GMPInterface
{
    public function add(GMP $second): GMPInterface;

    public function count(): int;

    public function getNumber(): array;

    public function __toString(): string;
}
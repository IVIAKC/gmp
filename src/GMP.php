<?php

namespace App;

use ArrayAccess;
use Countable;
use DomainException;

class GMP implements GMPInterface, Countable, ArrayAccess
{
    protected const DIVISOR = 10;

    /** @var array */
    protected $number = [];

    public function __construct(string $number)
    {
        $this->setNumber($number);
    }

    public function count(): int
    {
        return count($this->number);
    }

    public function add(GMP $second): GMPInterface
    {
        $firstLength = count($this);
        $secondLength = count($second);

        $firstNumber = array_reverse($this->getNumber());
        $secondNumber = array_reverse($second->getNumber());

        $maxLength = max($firstLength, $secondLength);

        $residue = $index = 0;
        $result = [];
        do {
            if ($index < $firstLength) {
                $residue += (int)$firstNumber[$index];
            }

            if ($index < $secondLength) {
                $residue += (int)$secondNumber[$index];
            }

            array_push($result, $residue % static::DIVISOR);

            $residue = intdiv($residue, static::DIVISOR);

            $index++;
        } while ($index < $maxLength || $residue > 0);

        return GMPCreator::create(array_reverse($result));
    }

    public function getNumber(): array
    {
        return $this->number;
    }

    protected function setNumber(string $number): void
    {
        $this->validation($number);

        $number = ltrim($number, '0');
        if ($number === '') {
            $number = '0';
        }

        $this->number = str_split($number);
    }

    protected function validation(string $number): void
    {
        $pattern = "/[^0-9]/";
        if (preg_match($pattern, $number) !== 0) {
            throw new DomainException('В строке должны быть только цифры!');
        }

        if (strlen($number) === 0) {
            throw new DomainException('Строка не должна быть пустой!');
        }

    }

    public function __toString(): string
    {
        return implode($this->number, '');
    }

    public function offsetExists($key)
    {
        return isset($this->number[$key]);
    }

    public function offsetGet($key)
    {
        return $this->offsetExists($key) ? $this->number[$key] : null;
    }

    public function offsetSet($key, $value)
    {
        if (is_null($key)) {
            $this->number[] = $value;
        } else {
            $this->number[$key] = $value;
        }
    }

    public function offsetUnset($key)
    {
        unset($this->number[$key]);
    }
}
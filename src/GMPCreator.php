<?php


namespace App;


use InvalidArgumentException;

class GMPCreator
{
    public static function create($value): GMP
    {
        switch (gettype($value)) {
            case 'string':
                return static::createFromString($value);

            case 'array':
                return static::createFromArray($value);

            case 'integer':
                return static::createFromInteger($value);

            default:
                throw new InvalidArgumentException(
                    sprintf("Создание объекта из типа %s не поддерживается!", gettype($value))
                );
        }
    }

    protected static function createFromString(string $string): GMP
    {
        return new GMP($string);
    }

    protected static function createFromArray(array $array): GMP
    {
        return new GMP(implode($array, ''));
    }

    protected static function createFromInteger(int $integer): GMP
    {
        return new GMP(strval($integer));
    }
}
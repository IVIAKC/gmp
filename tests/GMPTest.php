<?php

namespace Tests;

use App\GMP;
use PHPUnit\Framework\TestCase;

class GMPTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     * @param string $firstNumber
     * @param string $secondNumber
     * @param string $expected
     */
    public function testAdd(string $firstNumber, string $secondNumber, string $expected): void
    {
        //arrange
        $first = new GMP($firstNumber);
        $second = new GMP($secondNumber);

        //act
        $result = $first->add($second);

        //assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider validationProvider
     * @param string $number
     * @param string $expected
     */
    public function testValidation(string $number, string $expected): void
    {
        //assert
        $this->expectException($expected);

        new GMP($number);
    }

    public function validationProvider(): array
    {
        return [
            ['1asd', \DomainException::class],
            ['', \DomainException::class],
        ];
    }

    public function additionProvider(): array
    {
        return [
            [
                '0',
                '0',
                '0',
            ],
            [
                '2',
                '22',
                '24',
            ],
            [
                '1111',
                '9',
                '1120',
            ],
            [
                '1',
                '999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999',
                '1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
            ]
        ];
    }
}
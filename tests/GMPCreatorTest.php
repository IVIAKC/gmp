<?php

namespace Tests;

use App\GMPCreator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class GMPCreatorTest extends TestCase
{
    /**
     * @dataProvider createProvider
     * @param string $number
     * @param string $expected
     */
    public function testCreate($number, string $expected): void
    {
        //arrange
        $first = GMPCreator::create($number);

        //assert
        $this->assertEquals($first, $expected);
    }

    /**
     * @dataProvider failCreateProvider
     * @param string $number
     * @param string $expected
     */
    public function testFailCreate($number, string $expected): void
    {
        //assert
        $this->expectException($expected);

        //arrange
        GMPCreator::create($number);

    }

    public function createProvider(): array
    {
        return [
            [
                [0,1,2,3],
                '123',
            ],
            [
                123,
                '123',
            ],
            [
                '123',
                '123',
            ],
        ];
    }

    public function failCreateProvider(): array
    {
        return [
            [
                1.1,
                InvalidArgumentException::class
            ],
        ];
    }
}